FROM openjdk:11-jdk-bullseye

ARG CROMWELL_VERSION=71
ARG UDOCKER_VERSION=1.3.1

RUN apt-get update && apt-get install -y \
      python \
      curl \
      openssl && \
      rm -rf /var/cache/apt/lists

RUN groupadd -r app && useradd --no-log-init -r -g app app

RUN mkdir /cromwell && chown -R app:app /cromwell

RUN mkdir /udocker && chown -R app:app /udocker

USER app

WORKDIR /udocker

ENV PATH=/udocker/udocker:${PATH}
ENV UDOCKER_DIR=/udocker

RUN wget https://github.com/indigo-dc/udocker/releases/download/v${UDOCKER_VERSION}/udocker-${UDOCKER_VERSION}.tar.gz -O udocker.tar.gz && \
    tar zxvf udocker.tar.gz && \
    udocker install && \
    rm udocker.tar.gz

WORKDIR /cromwell

RUN wget https://github.com/broadinstitute/cromwell/releases/download/${CROMWELL_VERSION}/cromwell-${CROMWELL_VERSION}.jar -O cromwell.jar

COPY --chown=app:app application.conf application.conf

CMD ["/bin/bash", "-c", "java -jar -Dconfig.file=application.conf cromwell.jar server"]
